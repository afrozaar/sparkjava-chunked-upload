package com.afrozaar.sparkjava.chunkedupload;

import static org.junit.Assert.fail;

import static java.lang.Integer.parseInt;

import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author johan
 */
public class SparkAppTest {

    @Test
    public void SetCompare() {
        TreeSet<Integer> set1 = new TreeSet<>(Arrays.asList(1, 2, 3, 5, 4));

        TreeSet<Integer> set2 = new TreeSet<>(Arrays.asList(parseInt("1"), parseInt("2"), parseInt("3"), parseInt("4"), parseInt("5")));

        System.out.println("set1.equals(set2) = " + set1.equals(set2));

        System.out.println("set2.last = " + set2.last());

        final Set<Integer> collect = IntStream.range(1, set1.size()+1).boxed().collect(Collectors.toSet());
        System.out.println(collect);

        System.out.println(set1.equals(collect));

        if (!set1.equals(set2)) {
            fail();
        }
    }
}