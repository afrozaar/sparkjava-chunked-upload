package com.afrozaar.sparkjava.chunkedupload;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.Spark;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author johan
 */
public class SparkApp {

    private static final Logger LOG = LoggerFactory.getLogger(SparkApp.class);

    interface Headers {
        String HEADER_UPLOAD_ID = "x-afro-upload-id";
        String HEADER_UPLOAD_PART = "x-afro-upload-part";
    }

    public static void main(String[] args) {
        new SparkApp().init();
    }

    public void init() {

        LOG.info("Initialising SparkApp...");

        Spark.staticFileLocation("/public");

        post("/file/upload", "multipart/form-data", (request, response) -> {

            final byte[] bytes = request.bodyAsBytes();

            final String uploadId = request.headers(Headers.HEADER_UPLOAD_ID);
            final String uploadPart = request.headers(Headers.HEADER_UPLOAD_PART);

            registerForUpload(uploadId, uploadPart);
            storeBytes(uploadId, uploadPart, bytes);
            registerCompletedUpload(uploadId, uploadPart);

            if (canAssemble(uploadId)) {

                final File file = assembleParts0(uploadId);

                Map<String, Object> responseMap = new LinkedHashMap<>();

                responseMap.put("statusCode", 200);
                responseMap.put("destination file", file.getName());

                return responseMap;
            } else {
                return "waiting for more...";
            }
        });
    }

    private synchronized boolean canAssemble(String uploadId) {
        return gotAllBytes(uploadId) && !assembling(uploadId);
    }

    private Map<String, TreeSet<Integer>> registeredUploadMap = new HashMap<>();
    private synchronized void registerForUpload(String uploadId, String uploadPart) {
        final TreeSet<Integer> partSet = registeredUploadMap.getOrDefault(uploadId, new TreeSet<>());
        partSet.add(Integer.parseInt(uploadPart));
        registeredUploadMap.put(uploadId, partSet);
    }

    private Map<String, TreeSet<Integer>> completedUploadMap = new HashMap<>();
    private synchronized void registerCompletedUpload(String uploadId, String uploadPart) {
        LOG.info("Completed {}-{} @ {}", uploadId, uploadPart, Instant.now());
        final TreeSet<Integer> partSet = completedUploadMap.getOrDefault(uploadId, new TreeSet<>());
        partSet.add(Integer.parseInt(uploadPart));
        completedUploadMap.put(uploadId, partSet);
    }

    private Set<String> assemblingIds = new HashSet<>();

    private synchronized boolean assembling(String uploadId) {
        return assemblingIds.contains(uploadId);
    }

    private File assembleParts0(final String uploadId) {
        assemblingIds.add(uploadId);

        try {
            final File destinationFile = new File(Files.uploadDir.apply(uploadId), uploadId);
            OutputStream outputStream = new FileOutputStream(destinationFile, true);

            completedUploadMap.get(uploadId).stream().map(partId -> Files.uploadTargetFile.apply(uploadId, partId)).forEachOrdered(file -> {
                try {
                    outputStream.write(java.nio.file.Files.readAllBytes(Paths.get(file.toURI())));
                } catch (IOException e) {
                    LOG.error("Error appending {}", file, e);
                }
            });
            outputStream.flush();
            outputStream.close();
            return destinationFile;
        } catch (Exception e) {
            LOG.error("Error assembling {} ", uploadId, e);
        } finally {
            assemblingIds.remove(uploadId);
        }
        throw new IllegalStateException("Did not finish assembling " + uploadId);
    }

    private synchronized boolean gotAllBytes(String uploadId) {
        final TreeSet<Integer> registeredParts = registeredUploadMap.getOrDefault(uploadId, new TreeSet<>());
        final TreeSet<Integer> completedParts = completedUploadMap.getOrDefault(uploadId, new TreeSet<>());

        final Integer last = completedParts.isEmpty() ? null : completedParts.last();
        final Boolean lastFile = ofNullable(last).map(partId -> Files.uploadTargetFile.apply(uploadId, last).length() == 0).orElse(Boolean.FALSE);

        return !registeredParts.isEmpty() && !completedParts.isEmpty() && lastFile && completedParts.equals(IntStream.range(0, last + 1).boxed().collect(Collectors.toSet()));
    }

    private void storeBytes(String uploadId, String uploadPart, byte[] bytes) {
        LOG.debug("Storing {} part {}, size: {}", uploadId, uploadPart, bytes.length);

        try {
            File uploadFile = Files.uploadTargetFile.apply(uploadId, Integer.parseInt(uploadPart));
            Files.verifyDirectoryExists.accept(uploadFile.getParentFile());

            OutputStream outputStream = new FileOutputStream(uploadFile);
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();

            registerCompletedUpload(uploadId, uploadPart);
        } catch (IOException e) {
            LOG.error("Error storing {}-{}", uploadId, uploadPart, e);
        }
    }

    static class Files {
        static final Function<String, File> uploadDir = uploadId -> new File(new File(System.getProperty("java.io.tmpdir"), "afr-uploads"), uploadId);

        static final Consumer<File> verifyDirectoryExists = directory -> {
            if (!directory.exists() && !directory.mkdirs()) {
                LOG.error("Could not ensure that {} exists.", directory);
            }
        };

        static final BiFunction<String, Integer, File> uploadFile = (uploadId, uploadPart) -> new File(format("%s-%s", uploadId, uploadPart));

        static final BiFunction<String, Integer, File> uploadTargetFile = (uploadId, uploadPart) -> new File(uploadDir.apply(uploadId), uploadFile.apply(uploadId, uploadPart).getName());
    }
}
